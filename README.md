# react base framework

<div>
    <a href="https://nodejs.org/zh-tw">
        <img src="https://img.shields.io/badge/node-20.18.3-blue">
    </a>
    <a href="https://react.dev/">
        <img src="https://img.shields.io/badge/react-19.0.0-blue">
    </a>
    <a href="https://react.dev/">
        <img src="https://img.shields.io/badge/react dom-19.0.0-blue">
    </a>
    <a href="https://redux-toolkit.js.org/">
        <img src="https://img.shields.io/badge/@reduxjs/toolkit-2.5.1-blue">
    </a>
    <a href="https://jestjs.io/">
        <img src="https://img.shields.io/badge/jest-29.7.0-blue">
    </a>
    <a href="https://www.cypress.io/">
        <img src="https://img.shields.io/badge/cypress-14.0.3-blue">
    </a>
</div>

## 簡介

react base framework 是一個幫助新手所建立的架構，它基於 react 實建，它使用最新前端技術，提供完整的架構建置，可以幫助您快速建立企業級的架構，相信不管遇到什麼需求，這個架構可以幫助您，建置完美架構。

## 開發

```bash
# 克隆項目
git clone https://gitlab.com/timmyBai/react_base_framework.git

# 進入目錄
cd react_base_framework

# 安裝依賴
npm install
or
yarn install

# 啟用專案
# development
npm run start
or
yarn start

# staging
npm run start:stag
or
yarn start:stag

# production
npm run start:prod
or
yarn start:prod
```

## 打包

```bash
# development
npm run build
或
yarn build

# staging
npm run build:stag
或
yarn build:stag

# production
npm run build:prod
或
yarn build:prod
```

## 打包 Docker

```docker
# development
docker compose -f docker-compose.development.yml up --build -d

# staging
docker compose -f docker-compose.staging.yml up --build -d

# production
docker compose -f docker-compose.production.yml up --build -d
```

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |

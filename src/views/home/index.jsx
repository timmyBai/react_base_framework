// react
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

// css
import './index.sass';

const Home = () => {
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch({ type: 'loading/changeLoading', payload: true });
        let count = 0;
        const interval = setInterval(() => {
            count++;
            
            if (count === 3) {
                dispatch({ type: 'loading/changeLoading', payload: false });
                clearInterval(interval);
            }
        }, 1000);
    }, []);

    return (
        <div className='home'>
            This is home view
        </div>
    );
};

export default Home;
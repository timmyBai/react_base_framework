import defaultSettings from '@/settings.js';

const title = defaultSettings.title || 'react base framework';

export const getPageTitle = (pageTitle) => {
    if (pageTitle) {
        return `${pageTitle} - ${title}`;
    }

    return `${title}`;
};
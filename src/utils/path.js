export const resolvePath = (...parts) => {
    return parts.join('/').replace(/\/+/g, '/');
};

import React, { useContext } from 'react';

// css
import './MenuItem.sass';

// context
import MenuContext from './MenuContext';

const MenuItem = ({ children }) => {
    const menuContext = useContext(MenuContext);

    if (menuContext !== undefined) {
        return (
            <li className={`menuItem ${menuContext.type === 'subMenu'? 'subMenu': ''}`}>
                { children }
            </li>
        );
    }
    
    return (
        <li className='menuItem'>
            { children }
        </li>
    );
};

export default MenuItem;
import React from 'react';

// css
import './Menu.sass';

const Menu = ({ className, children }) => {
    return (
        <ul className={`menu ${className}`}>
            {children}
        </ul>
    );
};

export default Menu;
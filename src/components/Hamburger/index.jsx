import React from 'react';

// css
import './index.sass';

const Hamburger = ({ className, collapse }) => {
    return (
        <div className={`hamburger ${className}`} onClick={collapse}>
            <div className='hamburger_toggle'></div>
            <div className='hamburger_toggle'></div>
            <div className='hamburger_toggle'></div>
        </div>
    );
};

export default Hamburger;
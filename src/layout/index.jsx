// react
import React from 'react';

// mixins
import ResetHander from './mixins/ResetHander';

// components
import Navbar from './components/Navbar/index.jsx';
import AppMain from './components/AppMain/index.jsx';

const Layout = () => {
    ResetHander();

    return (
        <div className='appWapper'>
            <Navbar></Navbar>
            <AppMain></AppMain>
        </div>
    );
};

export default Layout;
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

const { body } = document;
const WIDTH = 992;

const ResetHander = () => {
    const location = useLocation();
    const dispatch = useDispatch();
    const storeGetters = useSelector(state => {
        return {
            sidebar: state.app.sidebar,
            device: state.app.device
        };
    });

    useEffect(() => {
        if ($_isMobile()) {
            dispatch({ type: 'app/toggleDevice', payload: 'mobile' });
            dispatch({ type: 'app/closeSideBar', payload: { withoutAnimation: true }});
        }
        else {
            dispatch({ type: 'app/toggleDevice', payload: 'desktop' });
            dispatch({ type: 'app/closeSideBar', payload: { withoutAnimation: true } });
        }
    }, []);

    useEffect(() => {
        window.addEventListener('resize', $_resizeHander);

        return () => {
            window.removeEventListener('resize', $_resizeHander);
        };
    }, []);

    useEffect(() => {
        if (storeGetters.device === 'mobile' && storeGetters.sidebar.opened) {
            dispatch({ type: 'app/closeSideBar', payload: { withoutAnimation: false }});
        }
    }, [location]);

    const $_isMobile = () => {
        const rect = body.getBoundingClientRect();
        return rect.width - 1 < WIDTH;
    };

    const $_resizeHander = () => {
        const isMobile = $_isMobile();
        dispatch({ type: 'app/toggleDevice', payload: isMobile ? 'mobile' : 'desktop'});

        if (isMobile) {
            dispatch({ type: 'app/closeSideBar', payload: { withoutAnimation: true }});
        }
    };
};

export default ResetHander;
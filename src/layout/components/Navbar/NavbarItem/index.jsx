import React from 'react';

// utils
import { isExternal } from '@/utils/validate.js';
import { resolvePath as resPath } from '@/utils/path';

// css
import './index.sass';

// components
import MenuItem from '@/components/Menu/MenuItem.jsx';
import SubMenu from '@/components/Menu/SubMenu';
import AppLink from '../AppLink/index.jsx';

let onlyOneChild = null;

const hasOneShowingChild = (children = [], parent) => {
    const showingChildren = children.filter(item => {
        if (item.hidden) {
            return false;
        }
        else {
            onlyOneChild = item;
            return true;
        }
    });

    if (showingChildren.length === 1) {
        return true;
    }

    if (showingChildren.length === 0) {
        onlyOneChild = { ...parent, path: '', noShowingChildren: true };
        return true;
    }
    
    return false;
};

const resolvePath = (routePath, basePath) => {
    if (isExternal(routePath)) {
        return routePath;
    }

    if (isExternal(basePath)) {
        return basePath;
    }
    
    const resolved = resPath(basePath, routePath).replace(/\/{2,}/g, '/');

    return resolved === '/' ? '/' : resolved.replace(/\/$/, '');
};

const NavbarItem = ({basePath, item, isNest}) => {
    if (!item.hidden) {
        if (hasOneShowingChild(item.children, item) && (!onlyOneChild.children || onlyOneChild.noShowingChildren) && !item.alwaysShow) {
            return (
                <MenuItem>
                    <AppLink to={resolvePath(onlyOneChild.path, basePath)} title={onlyOneChild.meta.title} isNest={isNest}></AppLink>
                </MenuItem>
            );
        }
        else {
            return (
                <SubMenu title={item.meta.title}>
                    {
                        item.children.map((child) => {
                            return (
                                <NavbarItem basePath={resolvePath(child.path, basePath)} item={child} isNest={true} key={child.path}></NavbarItem>
                            );
                        })
                    }
                </SubMenu>
            );
        }
    }
};

export default NavbarItem;
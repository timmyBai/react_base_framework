import React from 'react';
import { NavLink } from 'react-router-dom';

// css
import './index.sass';

const Logo = () => {
    return (
        <div className='logo'>
            <NavLink className='link' to='/'>react base framework</NavLink>
        </div>
    );
};

export default Logo;
import React from 'react';
import { NavLink } from 'react-router-dom';

// css
import './index.sass';

const AppLink = ({ to, title, isNest }) => {
    return (
        <NavLink className={isNest ? 'nestLink' : 'link'} to={to}>{title}</NavLink>
    );
};

export default AppLink;
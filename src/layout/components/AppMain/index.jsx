import React from 'react';
import { Outlet } from 'react-router-dom';

// css
import './index.sass';

const AppMain = () => {
    return (
        <section className='appMain'>
            <Outlet></Outlet>
        </section>
    );
};

export default AppMain;
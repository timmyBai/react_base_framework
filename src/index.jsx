import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import reportWebVitals from './reportWebVitals';
import 'normalize.css';

// css
import './styles/index.sass';

// js
import store from '@/store/index.js';

// component
import App from './App.jsx';
import Permission from './Permission.jsx';
import Router from './router/index.jsx';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <App>
                <Router>
                    <Permission></Permission>
                </Router>
            </App>
        </Provider>
    </React.StrictMode>
);

reportWebVitals();
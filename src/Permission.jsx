import React, { Fragment, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const Permission = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const [token] = useState(true);

    const handlePermission = () => {
        const whiteList = ['/404'];

        if (token) {
            navigate(location.pathname);
        }
        else {
            if (whiteList.indexOf(location.pathname) !== -1) {
                navigate(location.pathname);
            }
            else {
                navigate('/404');
            }
        }
    };

    useEffect(() => {
        handlePermission();
    }, [location.pathname, token]);

    return <Fragment></Fragment>;
};

export default Permission;
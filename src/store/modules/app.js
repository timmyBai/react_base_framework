import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isLoading: false,
    sidebar: {
        opened: false,
        withoutAnimation: false
    },
    device: ''
};

const reducers = {
    changeLoading: (state = initialState, action) => {
        state.isLoading = action.payload;
    },
    toggleSideBar: (state = initialState, action) => {
        state.sidebar.opened = !state.sidebar.opened;
        state.sidebar.withoutAnimation = false;
    },
    toggleDevice: (state = initialState, action) => {
        state.device = action.payload;
    },
    closeSideBar: (state = initialState, action) => {
        state.sidebar.opened = false;
        state.sidebar.withoutAnimation = action.payload.withoutAnimation;
    }
};

const appSlice = createSlice({
    name: 'app',
    initialState,
    reducers
});

const app = appSlice.reducer;

export default {
    app
};

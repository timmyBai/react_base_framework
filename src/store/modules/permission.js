import { createSlice } from '@reduxjs/toolkit';
import { constantRoutes } from '@/router/index.jsx';

const initialState = {
    routes: constantRoutes
};

const reducers = {
};

const permissionSlice = createSlice({
    name: 'permission',
    initialState,
    reducers
});

const permission = permissionSlice.reducer;

export default {
    permission
};

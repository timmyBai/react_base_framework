import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isLoading: false
};

const reducers = {
    changeLoading: (state = initialState, action) => {
        state.isLoading = action.payload;
    }
};

const loadingSlice = createSlice({
    name: 'loading',
    initialState,
    reducers
});

const loading = loadingSlice.reducer;

export default {
    loading
};

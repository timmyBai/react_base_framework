import { configureStore } from '@reduxjs/toolkit';
// import logger from 'redux-logger';
// import thunk from 'redux-thunk';

const modulesFiles = require.context('./modules', true, /\.js/);

const modules = modulesFiles.keys().reduce((modules, modulePath) => {
    const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1');

    const value = modulesFiles(modulePath);

    modules[moduleName] = value.default[moduleName];

    return modules;
}, {});

const store = configureStore({
    reducer: {
        ...modules
    },
    middleware: getDefaultMiddleware => getDefaultMiddleware({
        serializableCheck: false
    })
});

export default store;

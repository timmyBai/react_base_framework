// react
import React from 'react';
import { useSelector } from 'react-redux';

// css
import './App.sass';

// components
import Loading from '@/components/Loading/index.jsx';

const App = ({ children }) => {
    const storeGetters = useSelector(state => {
        return {
            isLoading: state.loading.isLoading
        };
    });

    return (
        <div className='App'>
            { children }
            { storeGetters.isLoading && <Loading></Loading> }
        </div>
    );
};

export default App;

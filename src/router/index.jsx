// react
import React from 'react';
import { BrowserRouter, Navigate } from 'react-router-dom';
import RouterWaiter from './components/RouterWaiter';
import { getPageTitle } from '@/utils/getPageTitle';

// layout
import Layout from '@/layout/';
import Home from '@/views/home/index';
import D3 from '@/views/lab/d3';
import Three from '@/views/lab/three';
import About from '@/views/about/index';
import Contact from '@/views/contact/index';
import ErrorPage404 from '@/views/errorPage/errorPage404';

// https://www.npmjs.com/package/react-router-waiter
export const constantRoutes = [
    {
        path: '',
        element: <Layout />,
        children: [
            {
                path: '',
                index: true,
                element: <Home />,
                meta: {
                    title: '首頁'
                }
            }
        ]
    },
    {
        path: '/lab',
        element: <Layout />,
        meta: {
            title: '實驗室'
        },
        alwaysShow: true,
        children: [
            {
                path: 'd3',
                element: <D3 />,
                meta: {
                    title: 'd3.js'
                }
            },
            {
                path: 'three',
                element: <Three />,
                meta: {
                    title: 'three.js'
                }
            }
        ]
    },
    {
        path: '/about',
        element: <Layout />,
        children: [
            {
                path: '',
                element: <About />,
                meta: {
                    title: '關於'
                }
            }
        ]
    },
    {
        path: '/contact',
        element: <Layout />,
        children: [
            {
                path: '',
                element: <Contact />,
                meta: {
                    title: '聯繫'
                }
            }
        ]
    },
    {
        path: '/404',
        element: <ErrorPage404 />,
        meta: {
            title: '404'
        },
        hidden: true
    },
    {
        path: '*',
        element: <Navigate to='/404' />,
        hidden: true
    }
];

// permission routes
export const asyncRoutes = [

];

// router components
const Routers = (props) => {
    const onRouteBefore = ({ pathname, meta }) => {
        document.title = getPageTitle(meta.title);
    };

    return (
        <BrowserRouter>
            <RouterWaiter routes={constantRoutes} onRouteBefore={onRouteBefore} />
            { props.children }
        </BrowserRouter>
    );
};

export default Routers;
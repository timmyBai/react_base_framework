const { defineConfig } = require('cypress');
const packageJson = require('./package.json');

const startTime = new Date();

module.exports = defineConfig({
    e2e: {
        baseUrl: 'http://localhost:3000',
        viewportWidth: 1920,
        viewportHeight: 932,
        experimentalSourceRewriting: false,
        videosFolder: 'tests/cypress/videos',
        fixturesFolder: 'tests/cypress/fixtures',
        downloadsFolder: 'tests/cypress/downloads',
        screenshotsFolder: 'tests/cypress/screenshots',
        specPattern: 'tests/cypress/e2e/**/*.cy.{js,jsx,ts,tsx}',
        supportFile: 'tests/cypress/support/e2e.js',
        setupNodeEvents(on, config) {
            const { baseUrl } = config;
            const endTime = new Date();
            const elapsedTimeInSeconds = endTime - startTime;

            on('before:run', () => {
                console.clear();
                console.log(`\x1B[32m➜  CYPRESS v${packageJson.version} \x1B[37m ready in ${elapsedTimeInSeconds}ms`);
                console.log();
                console.log(`\x1B[32m➜  E2E Local:\t\x1B[36m${baseUrl}`);
                console.log('\x1B[32m➜  Network:\t\x1B[37muse --host to expose');
                console.log();
                console.log(`You can now view ${packageJson.name} in the browser, and view tests result.`);
            });

            on('after:run', () => {
                console.log('E2E Tests Exit');
            });
        }
    }
});

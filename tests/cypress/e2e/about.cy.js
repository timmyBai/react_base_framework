describe('test about page', () => {
    it('test about page', () => {
        cy.visit('/about');

        // 檢查有無 navbar 有無 next base framework 字眼連結
        cy.get('.appWapper .navbar .container .row .d_float_left .logo .link').should('exist');

        cy.wait(5000);
        cy.get('.appWapper .navbar .container .row .d_float_left .logo .link').should('exist').click();
        
        // 確認點擊 '首頁' 有轉跳到 / 路由
        cy.get('.appWapper .navbar .container .row div.d_float_right ul.menu.close.desktop li.menuItem:nth-child(1) a.link').should('exist').click();
        cy.url().should('include', '/');
        cy.get('.appMain .home').should('contain', 'This is home view');

        // 確認點擊 '關於' 有轉跳到 /about 路由
        cy.get('.appWapper .navbar .container .row div.d_float_right ul.menu.close.desktop li.menuItem:nth-child(3) a.link').should('exist').click();
        cy.url().should('include', '/about');
        cy.get('.appMain .about').should('contain', 'This is about view');

        // 確認點擊 '關於' 有轉跳到 /contact 路由
        cy.get('.appWapper .navbar .container .row div.d_float_right ul.menu.close.desktop li.menuItem:nth-child(4) a.link').should('exist').click();
        cy.url().should('include', '/contact');
        cy.get('.appMain .contact').should('contain', 'This is contact view');
    });

    it('test lab/d3 router page', () => {
        // 確認 '實驗室/d3.js' 有轉跳到 /lab/d3 路由
        cy.visit('/lab/d3');

        cy.url().should('include', '/lab/d3');
        cy.get('.appMain .d3').should('contain', 'This is d3.js view');
    });

    it('test lab/three router page', () => {
        // 確認 '實驗室/three.js' 有轉跳到 /lab/three 路由
        cy.visit('/lab/three');

        cy.url().should('include', '/lab/three');
        cy.get('.appMain .three').should('contain', 'This is three.js view');
    });
});
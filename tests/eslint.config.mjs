const config = [
    {
        languageOptions: {
            globals: {
                jest: true
            }
        }
    }
];

export default config;